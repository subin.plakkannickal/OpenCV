echo "installing depedencies......"
echo "##########################################################################"
apt-get update && apt-get upgrade
apt-get -y install build-essential cmake pkg-config
apt-get -y install libjpeg62-dev
apt-get -y install libjasper-dev
apt-get -y install libtiff5-dev:i386 libtiff5-dev
apt-get -y install  libgtk2.0-dev
apt-get -y install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
apt-get -y install libdc1394-22-dev
apt-get -y install libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
apt-get -y install libxine2-dev
apt-get -y install python-dev python-numpy
apt-get -y install libqt4-dev libqt5-dev
apt-get -qq install libopencv-dev build-essential checkinstall cmake pkg-config yasm libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine2 libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev python-dev python-numpy libtbb-dev libqt4-dev libgtk2.0-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils
echo "dependencies installed"
echo "##########################################################################"
echo "opencv downloading..."
wget http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/2.4.13/opencv-2.4.13.zip
echo "download completed."
echo "##########################################################################"
echo " unzipping opencv..."
unzip opencv-2.4.13.zip
echo "##########################################################################"
cd opencv-2.4.13
mkdir build
cd build
echo "##########################################################################"
echo "cmake..."
cmake -D CMAKE_BUILD_TYPE=RELEASE -D MAKE_INSTALL_PREFIX=/usr/local -D  WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=ON -D WITH_OPENGL=ON ..
echo "##########################################################################"
echo "making..."
make
echo "##########################################################################"
echo "open cv installiang..."
echo "##########################################################################"
make install
echo "##########################################################################"
echo "opencv installation completed"
echo "##########################################################################"
cd ../..
rm -rf opencv-2.4.13 opencv-2.4.13.zip 
echo "opencv directory removed!!!"
